//@@author Limminghong
package seedu.address.model.backup;

/**
 * Represents a list of all the snapshots of backups.
 */
public class BackupList {
    private final String value;

    public BackupList(String value) {
        this.value = value;
    }
}
